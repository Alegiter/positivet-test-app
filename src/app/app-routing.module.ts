import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./componetns/login/login.component";
import {CurrencyCalculatorComponent} from "./componetns/currency-calculator/currency-calculator.component";

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'currency-calculator', component: CurrencyCalculatorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
