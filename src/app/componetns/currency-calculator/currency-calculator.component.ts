import { Component, OnInit } from '@angular/core';
import {CurrencyConverter} from "../../services/currency-converter.service";
import {forkJoin} from "rxjs";
import {CartItem} from "../../_source/cart-item";
import {Currency} from "../../_source/currency";

@Component({
  selector: 'app-currency-calculator',
  templateUrl: './currency-calculator.component.html',
  styleUrls: ['./currency-calculator.component.css'],
  providers: [CurrencyConverter]
})
export class CurrencyCalculatorComponent implements OnInit {

  private readonly tag = CurrencyCalculatorComponent.name;

  private cart:CartItem[] = [
    {price:20},
    {price:45},
    {price:67},
    {price:1305},
  ];

  private totalPrice:number;
  private totalCartPrices;

  constructor(private currencyConverter:CurrencyConverter) {}

  ngOnInit() {
    this.totalPrice = this.calculateTotalCartPrice(this.cart);

    this.calculateAllPrices();
  }

  calculateTotalCartPrice(cart:CartItem[]) {
    const sum = (acc, item:CartItem) => acc + item.price;
    return cart.reduce(sum, 0);
  }

  calculateAllPrices() {
    const defaultCurrency:Currency = "USD";
    const currenciesMap = this.currencyConverter.currencies.map(c => `${defaultCurrency}_${c}`);
    const requestMap = currenciesMap.map(c => this.currencyConverter.convert(c));

    forkJoin(requestMap)
      .subscribe(
        (responseCurrencies:Array<{}>) => {

          const totalPrices = currenciesMap.map(currency => {
            const currencyVal = responseCurrencies.find(rc => rc[currency])[currency];
            const total = this.currencyConverter.round(currencyVal * this.totalPrice);
            return {[currency]: total}
          });

          this.totalCartPrices = Object.assign({}, ...totalPrices);
          console.log(this.totalCartPrices);
        },
        error1 => console.error(`${this.tag} calculateAllPrices `, error1)
      );
  }

}
