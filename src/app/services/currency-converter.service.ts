import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";
import {Currency} from "../_source/currency";

@Injectable()
export class CurrencyConverter {

  private readonly tag = CurrencyConverter.name;

  private readonly apiKey:string = "4ba410a0614e01f03ce1";
  readonly currencies:Currency[] = ["USD", "RUB", "EUR", "GBP", "JPY"];

  constructor(private http:HttpClient) {}

  convert(q:string):Observable<any> {
    return this.http.get(`https://free.currconv.com/api/v7/convert?q=${q}&compact=ultra&apiKey=${this.apiKey}`)
      .pipe(
        catchError(e => {
          console.error(this.tag + ` convert(${q}) `, e);
          return throwError(e);
        })
      );
  }

  round(value:number) {
    return Math.round(value * 100) / 100;
  }

}
