import { TestBed } from '@angular/core/testing';

import { CurrencyConverter } from './currency-converter.service';

describe('CurrencyConverter', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CurrencyConverter = TestBed.get(CurrencyConverter);
    expect(service).toBeTruthy();
  });
});
